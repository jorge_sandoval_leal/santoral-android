# Santoral para Android

El Santoral para Android es una aplicación sencilla donde poder los santos de un día concreto o buscar la fecha en que estará de santo una persona. Los datos se recogen de la web [Santopedia.com][4] y se muestran directamente al usuario.

Esta aplicación usa las siguientes librerias:

  - [ActionBarSherlock][1]
  - [Jackson JSON Processor][3]
  - [Google AdMob SDK][2]


El código fuente está licenciado bajo la licencia WTFPL-2 para que cada uno pueda coger lo que necesite.

Si vas a usar el código tal cual recuerda solicitar el acceso a la API de la web y cambiar esos valores en la clase PrivateData.



[1]:http://actionbarsherlock.com
[2]:http://developers.google.com/mobile-ads-sdk
[3]:http://wiki.fasterxml.com/JacksonHome
[4]:http://www.santopedia.com