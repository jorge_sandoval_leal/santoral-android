package es.kix2902.santoral;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Collections;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.kix2902.santoral.wheel.WheelView;
import es.kix2902.santoral.wheel.adapters.ArrayWheelAdapter;

public class Main extends SherlockListActivity {
	private String paramKey;

	private int month, day;
	Calendar cal;

	String meses[];

	private CustomAdapter adapter;
	private ProgressDialog progress = null;

	private ActionBar actionbar;
	private MenuItem menuSearch;
	private EditText editSearch;
	private ImageButton btnSearch;

	private ItemList itemList;

	String wheelMeses[];
	String wheelDias[] = new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
			"27", "28", "29", "30", "31" };

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		setContentView(R.layout.main);

		paramKey = "?app_id=" + PrivateData.getAppId() + "&app_key=" + PrivateData.getAppKey();

		actionbar = getSupportActionBar();

		meses = getResources().getStringArray(R.array.meses_array);

		cal = Calendar.getInstance();
		month = cal.get(Calendar.MONTH);
		day = cal.get(Calendar.DAY_OF_MONTH);

		actionbar.setSubtitle(day + " - " + meses[month]);

		itemList = new ItemList();
		adapter = new CustomAdapter(this, R.layout.main, itemList);
		setListAdapter(adapter);

		updateDay();

		wheelMeses = getResources().getStringArray(R.array.meses_array);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.ab_menu, menu);

		menuSearch = menu.findItem(R.id.menu_search);
		menuSearch.setOnActionExpandListener(expandListener);

		return true;
	}

	OnActionExpandListener expandListener = new OnActionExpandListener() {

		@Override
		public boolean onMenuItemActionExpand(MenuItem item) {
			editSearch = (EditText) item.getActionView().findViewById(R.id.editSearch);
			editSearch.setText("");
			editSearch.requestFocus();

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

			editSearch.setOnEditorActionListener(editActionServer);

			btnSearch = (ImageButton) item.getActionView().findViewById(R.id.btnSearch);
			btnSearch.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					editSearch.onEditorAction(EditorInfo.IME_ACTION_DONE);
				}
			});

			return true;
		}

		@Override
		public boolean onMenuItemActionCollapse(MenuItem item) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editSearch.getWindowToken(), 0);
			return true;
		}
	};

	OnEditorActionListener editActionServer = new OnEditorActionListener() {

		@Override
		public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event) {
			updateName();

			return false;
		}

	};

	@Override
	public boolean onSearchRequested() {
		menuSearch.expandActionView();

		return true;
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			int nday = cal.get(Calendar.DAY_OF_MONTH);
			int nmonth = cal.get(Calendar.MONTH);

			if ((nday != day) || (nmonth != month)) {
				day = nday;
				month = nmonth;

				actionbar.setSubtitle(day + " - " + meses[month]);

				updateDay();
			}

			return true;

		case R.id.menu_select:
			CreateDialogDay();

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private void updateName() {
		if (!isOnline()) {
			avisoNoConexion();

		} else {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editSearch.getWindowToken(), 0);

			menuSearch.collapseActionView();

			new AsyncDownloadName().execute();
			progress = ProgressDialog.show(Main.this, getResources().getString(R.string.espere), getResources().getString(R.string.obtener), true);
		}
	}

	private void updateDay() {
		if (!isOnline()) {
			avisoNoConexion();

		} else {
			new AsyncDownloadDay().execute();
			progress = ProgressDialog.show(Main.this, getResources().getString(R.string.espere), getResources().getString(R.string.obtener), true);
		}
	}

	private class AsyncDownloadDay extends AsyncTask<Void, Void, Void> {

		private String baseUrl = "http://api.santopedia.com/days/";

		AndroidHttpClient client;
		HttpGet request;

		ObjectMapper mapper;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			String url = baseUrl + String.format("%02d-%02d", month + 1, day) + paramKey;

			client = AndroidHttpClient.newInstance("Santoral/2.0/Android");
			request = new HttpGet(url);
			mapper = new ObjectMapper();

			actionbar.setHomeButtonEnabled(false);
			actionbar.setDisplayHomeAsUpEnabled(false);

			adapter.clear();
			itemList.clear();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				HttpResponse response = client.execute(request);

				InputStream is = response.getEntity().getContent();
				itemList = mapper.readValue(is, ItemList.class);

			} catch (JsonParseException e) {
				e.printStackTrace();

			} catch (JsonMappingException e) {
				e.printStackTrace();

			} catch (IOException e) {
				e.printStackTrace();
				avisoNoConexion();

			} finally {
				client.close();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			Collections.sort(itemList);
			adapter = new CustomAdapter(Main.this, R.layout.main, itemList);
			setListAdapter(adapter);

			if ((day == cal.get(Calendar.DAY_OF_MONTH)) && (month == cal.get(Calendar.MONTH))) {
				actionbar.setHomeButtonEnabled(false);
				actionbar.setDisplayHomeAsUpEnabled(false);

			} else {
				actionbar.setHomeButtonEnabled(true);
				actionbar.setDisplayHomeAsUpEnabled(true);
			}

			progress.dismiss();
		}
	}

	private class AsyncDownloadName extends AsyncTask<Void, Void, Void> {

		private String baseUrl = "http://api.santopedia.com/names/";

		AndroidHttpClient client;
		HttpGet request;

		ObjectMapper mapper;

		ItemName item;
		ItemListNames listNames;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			String name = editSearch.getText().toString().trim();
			name = name.replace(" ", "%20");
			String url = baseUrl + name + paramKey;

			client = AndroidHttpClient.newInstance("Santoral/2.0/Android");
			request = new HttpGet(url);
			mapper = new ObjectMapper();

			listNames = new ItemListNames();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				HttpResponse response = client.execute(request);

				InputStream is = response.getEntity().getContent();
				listNames = mapper.readValue(is, ItemListNames.class);

			} catch (JsonParseException e) {
				e.printStackTrace();

			} catch (JsonMappingException e) {
				e.printStackTrace();

			} catch (IOException e) {
				e.printStackTrace();
				avisoNoConexion();

			} finally {
				client.close();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			progress.dismiss();

			if (listNames.size() > 0) {
				item = listNames.get(0);

				Builder builder = new Builder(Main.this);
				builder.setTitle(item.getName());

				if (!item.getFeast().equals("")) {
					String fecha = item.getFeast();
					String[] data = fecha.split("-");
					String strMes = meses[Integer.valueOf(data[0]) - 1];

					builder.setMessage(item.getFullname() + "\n" + data[1] + " " + getResources().getString(R.string.de) + " " + strMes);

				} else {
					builder.setMessage(item.getFullname() + "\n" + getResources().getString(R.string.no_fecha_santo));

				}

				builder.setPositiveButton(R.string.mas_i, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String url = item.getUrl();
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

						dialog.dismiss();
						startActivity(intent);
					}
				});
				builder.setNegativeButton(R.string.cerrar, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				builder.create().show();

			} else {
				Toast.makeText(Main.this, R.string.no_santo, Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void CreateDialogDay() {

		AlertDialog.Builder builder = new Builder(Main.this);
		builder.setTitle(R.string.mnuSelect);

		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.dialog_wheels, null);
		layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		layout.setPadding(5, 0, 5, 0);
		
		final WheelView wheelDay = (WheelView) layout.findViewById(R.id.wheelDia);
		wheelDay.setViewAdapter(new ArrayWheelAdapter<String>(this, wheelDias));
		wheelDay.setVisibleItems(4);
		wheelDay.setCurrentItem(day - 1);

		final WheelView wheelMonth = (WheelView) layout.findViewById(R.id.wheelMes);
		wheelMonth.setViewAdapter(new ArrayWheelAdapter<String>(this, wheelMeses));
		wheelMonth.setVisibleItems(4);
		wheelMonth.setCurrentItem(month);

		builder.setView(layout);
		builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}

		});
		builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (!validDate(wheelDay.getCurrentItem() + 1, wheelMonth.getCurrentItem() + 1)) {
					Toast.makeText(Main.this, R.string.no_fecha, Toast.LENGTH_SHORT).show();

				} else {
					day = wheelDay.getCurrentItem() + 1;
					month = wheelMonth.getCurrentItem();

					actionbar.setSubtitle(day + " - " + meses[month]);
					updateDay();

					dialog.dismiss();
				}
			}
		});

		builder.create().show();
	}

	private Boolean validDate(int nday, int nmonth) {
		switch (nmonth) {
		case 4:
		case 6:
		case 9:
		case 11:
			if (nday == 31) {
				return false;
			} else {
				return true;
			}

		case 2:
			if (nday > 29) {
				return false;
			} else {
				return true;
			}

		default:
			return true;
		}
	}

	private void avisoNoConexion() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(Main.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
			}
		});
	}
}
