package es.kix2902.santoral;

public class PrivateData {

	private static final String APP_ID = "YOUR-APP-ID";
	private static final String APP_KEY = "YOUR-APP-KEY";

	public static String getAppId() {
		return APP_ID;
	}

	public static String getAppKey() {
		return APP_KEY;
	}

}
