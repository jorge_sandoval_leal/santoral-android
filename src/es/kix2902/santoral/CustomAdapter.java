package es.kix2902.santoral;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomAdapter extends ArrayAdapter<Item> implements OnClickListener {
	private ItemList items;
	Context context;

	public CustomAdapter(Context context, int resource, ItemList items) {
		super(context, resource, items);

		this.items = items;
		this.context = context;
	}

	@Override
	public View getView(int position, android.view.View convertView, android.view.ViewGroup parent) {

		ViewHolder holder;

		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = vi.inflate(R.layout.itemrow, null);

			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.txtName);
			holder.fullname = (TextView) convertView.findViewById(R.id.txtSanto);
			holder.important = (ImageView) convertView.findViewById(R.id.imgImportant);
			holder.itemRow = (LinearLayout) convertView.findViewById(R.id.layRow);
			convertView.setTag(R.id.VIEWHOLDER, holder);

		} else {
			holder = (ViewHolder) convertView.getTag(R.id.VIEWHOLDER);
		}

		Item item = items.get(position);
		if (item != null) {
			if (holder.name != null) {
				holder.name.setText(item.getName());
			}

			if (holder.fullname != null) {
				holder.fullname.setText(item.getFullname());
			}

			if (item.getImportant().equalsIgnoreCase("1")) {
				holder.important.setVisibility(View.VISIBLE);
			} else {
				holder.important.setVisibility(View.GONE);
			}

			holder.itemRow.setOnClickListener(this);
			convertView.setTag(R.id.LIST_POSITION, position);
		}

		return convertView;
	}

	static class ViewHolder {
		TextView name;
		TextView fullname;
		ImageView important;
		LinearLayout itemRow;
	}

	@Override
	public void onClick(View v) {
		int pos = (Integer) v.getTag(R.id.LIST_POSITION);
		String url = items.get(pos).getUrl();

		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		context.startActivity(intent);
	}

}
