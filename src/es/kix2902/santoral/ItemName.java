package es.kix2902.santoral;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemName {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("fullname")
	private String fullname;
	@JsonProperty("url")
	private String url;
	@JsonProperty("foto")
	private String foto;
	@JsonProperty("important")
	private String important;
	@JsonProperty("feast")
	private String feast;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getImportant() {
		return important;
	}

	public void setImportant(String important) {
		this.important = important;
	}

	public String getFeast() {
		return feast;
	}

	public void setFeast(String feast) {
		this.feast = feast;
	}

}
